import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  // URLovi za CAS i token manager su u environment objektu
  tokenManagerURL: string = environment.tokenManagerURL;
  casLoginURL: string = environment.casLoginURL;
  casLogoutURL: string = environment.casLogoutURL;

  // PODSETNIK *** aplikacija se pokrece komandom: ng serve --proxy-config proxy.conf.json

  jwtToken: string = '';

  constructor(private http: HttpClient){}

  ngOnInit(): void {
    this.http.get(this.tokenManagerURL, {responseType: 'text'}).subscribe(
      result => {
        this.jwtToken=result
        sessionStorage.setItem('jwtToken', result);
      },
      error => {
        console.log(error);
        if(error.status === 401) {
          window.location.replace(this.casLoginURL);
        }
        else {
          // handle-uje se kako god odlucis; mozda neki error page???
          console.log(error);
        }
      }
    );
  }

  logout(): void {
    sessionStorage.removeItem('jwtToken');
    window.location.replace(this.casLogoutURL);
  }
  
}
